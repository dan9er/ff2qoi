# farbfeld-converter-qoi version
VERSION = 1

# Copy config.def.mk to config.mk, then customize below to fit your system

# install paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

# flags
CPPFLAGS = -D_DEFAULT_SOURCE
CFLAGS   = -std=c99 -pedantic -Wall -Wextra
LDFLAGS  = -s

# release flags
CFLAGS   += -Os

# debug flags
#CFLAGS   += -O0 -g3
#LDFLAGS  += -g3

# compiler and linker
CC = cc
