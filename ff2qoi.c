/* See LICENSE.txt for copyright & license details. */
#include <arpa/inet.h>

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static uint_least8_t ff_read_subpixel(void);

static inline void ff_read_pixel(rgba8* p);

static inline void qoi_write_header(const uint32_t width,
	const uint32_t height);

static inline void ff2qoi(const uint32_t width, const uint32_t height);

static inline void usage(void);

static uint_least8_t
ff_read_subpixel(void)
{
	uint_least16_t tmp;

	efread(&tmp, sizeof(tmp), 1, stdin);
	tmp = ntohs(tmp);

	if ((tmp & 0x00ff) ^ (tmp >> 8))
		die("16-bit percise value detected, refusing to quantize");

	return (uint_least8_t)tmp;
}

static inline void
ff_read_pixel(rgba8* p)
{
	p->r = ff_read_subpixel();
	p->g = ff_read_subpixel();
	p->b = ff_read_subpixel();
	p->a = ff_read_subpixel();
}

static inline void
qoi_write_header(const uint32_t width, const uint32_t height)
{
	uint_least32_t tmp;

	efputs("qoif", stdout);

	tmp = htonl(width);
	efwrite(&tmp, sizeof(tmp), 1, stdout);

	tmp = htonl(height);
	efwrite(&tmp, sizeof(tmp), 1, stdout);

	efputc(4, stdout); /* RGBA */
	efputc(0, stdout); /* sRGB with linear alpha */
}

static inline void
ff2qoi(const uint32_t width, const uint32_t height)
{
	const uint64_t totalpix = (uint64_t)width * (uint64_t)height;

	        rgba8  curr,
	               prev = {0x00,0x00,0x00,0xff},
	               index[64],
	              *indexp;
	uint_least8_t  writebuf[5];
	      uint8_t  run = 0;

	struct {
		int_least8_t r, g, b;
	} diff;

	qoi_write_header(width, height);

	memset(index, 0x00000000, sizeof(index));

	for (uint64_t pix = 0; pix < totalpix; ++pix) {
		ff_read_pixel(&curr);

		/* RUN */
		if (!memcmp(&curr, &prev, sizeof(rgba8))) {
			++run;

			if (run == 62) {
				writebuf[0] = QOI_OP_RUN | (uint_least8_t)(run - 1);
				efwrite(&writebuf, sizeof(uint_least8_t), 1, stdout);

				run = 0;
			}

			continue;
		} else if (run) {
			writebuf[0] = QOI_OP_RUN | (uint_least8_t)(run - 1);
			efwrite(&writebuf, sizeof(uint_least8_t), 1, stdout);

			run = 0;

			/* fallthrough */
		}

		/* INDEX */
		indexp = index + qoi_hash(curr);
		if (!memcmp(&curr, indexp, sizeof(rgba8))) {
			writebuf[0] = QOI_OP_INDEX | (uint_least8_t)(indexp - index);
			efwrite(&writebuf, sizeof(uint_least8_t), 1, stdout);

			goto next_noindex;
		}

		/* RGBA */
		if (curr.a != prev.a) {
			writebuf[0] = QOI_OP_RGBA;
			memcpy(writebuf + 1, &curr, 4);
			efwrite(&writebuf, sizeof(uint_least8_t), 5, stdout);

			goto next;
		}

		/* DIFF */
		diff.r = curr.r - prev.r;
		diff.g = curr.g - prev.g;
		diff.b = curr.b - prev.b;
		if (   diff.r > -3 && diff.r < 2
		    && diff.g > -3 && diff.g < 2
		    && diff.b > -3 && diff.b < 2) {
			writebuf[0] =   QOI_OP_DIFF
			              | (((uint_least8_t)diff.r + 2) << 4)
			              | (((uint_least8_t)diff.g + 2) << 2)
			              |  ((uint_least8_t)diff.b + 2);
			efwrite(&writebuf, sizeof(uint_least8_t), 1, stdout);

			goto next;
		}

		/* LUMA */
		diff.r -= diff.g;
		diff.b -= diff.g;
		if (   diff.g > -33 && diff.g < 32
		    && diff.r > - 9 && diff.r <  8
			&& diff.b > - 9 && diff.b <  8) {
			writebuf[0] = QOI_OP_LUMA |  ((uint_least8_t)diff.g + 32);
			writebuf[1] =               (((uint_least8_t)diff.r +  8) << 4)
			                          |  ((uint_least8_t)diff.b +  8);
			efwrite(&writebuf, sizeof(uint_least8_t), 2, stdout);

			goto next;
		}

		/* RGB */
		writebuf[0] = QOI_OP_RGB;
		memcpy(writebuf + 1, &curr, 3);
		efwrite(&writebuf, sizeof(uint_least8_t), 4, stdout);

	next:
		*indexp = curr;

	next_noindex:
		prev = curr;
	}

	if (run) {
		writebuf[0] = QOI_OP_RUN | (uint_least8_t)(run - 1);
		efwrite(&writebuf, sizeof(uint_least8_t), 1, stdout);
	}

	efwrite("\x0\x0\x0\x0\x0\x0\x0\x1", sizeof(uint_least8_t), 8, stdout);
}

static inline void
usage(void)
{
	fprintf(stderr, "usage: %s", argv0);
	exit(1);
}

int
main(int argc, char *argv[])
{
	uint32_t width, height;

	argv0 = argv[0];
	if (argc != 1)
		usage();

	ff_read_header(&width, &height);

	ff2qoi(width, height);

	return fshut(stdout, "<stdout>");
}
