/* See LICENSE.txt and LICENSE_UTIL.txt for copyright & license details. */
#include <arpa/inet.h>

#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "util.h"

char *argv0;

static void
verr(const char *fmt, va_list ap)
{
	if (argv0)
		fprintf(stderr, "%s: ", argv0);

	vfprintf(stderr, fmt, ap);

	if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
}

void
warn(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	verr(fmt, ap);
	va_end(ap);
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	verr(fmt, ap);
	va_end(ap);

	exit(1);
}

void
ff_read_header(uint32_t *restrict width, uint32_t *restrict height)
{
	uint32_t header[4];

	efread(header, sizeof(*header), LEN(header), stdin);

	if (memcmp("farbfeld", header, sizeof("farbfeld") - 1))
		die("Invalid magic value");

	*width  = ntohl(header[2]);
	*height = ntohl(header[3]);
}

void
ff_write_header(const uint32_t width, const uint32_t height)
{
	uint32_t tmp;

	efputs("farbfeld", stdout);

	tmp = htonl(width);
	efwrite(&tmp, sizeof(tmp), 1, stdout);

	tmp = htonl(height);
	efwrite(&tmp, sizeof(tmp), 1, stdout);
}

int
fshut(FILE *restrict fp, const char *restrict fname)
{
	int ret = 0;

	/* fflush() is undefined for input streams by ISO C,
	 * but not POSIX 2008 if you ignore ISO C overrides.
	 * Leave it unchecked and rely on the following
	 * functions to detect errors.
	 */
	fflush(fp);

	if (ferror(fp) && !ret) {
		warn("ferror '%s':", fname);
		ret = 1;
	}

	if (fclose(fp) && !ret) {
		warn("fclose '%s':", fname);
		ret = 1;
	}

	return ret;
}

void
efread(void *restrict p, const size_t s, const size_t n, FILE *restrict f)
{
	if (fread(p, s, n, f) != n) {
		if (ferror(f))
			die("fread:");
		else
			die("fread: Unexpected end of file");
	}
}

void
efwrite(const void *restrict p, const size_t s, const size_t n,
	FILE *restrict f)
{
	if (fwrite(p, s, n, f) != n)
		die("fwrite:");
}

void
efputs(const char *restrict s, FILE *restrict f)
{
	if (fputs(s, f) == EOF)
		die("fputs:");
}

void
efputc(const int c, FILE *f)
{
	if (fputc(c, f) == EOF)
		die("fputc:");
}
